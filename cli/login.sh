set -e

. ./support/functions.sh

vault_iam_login() {
  signed_request=$(python support/sign-request.py vault.service.consul)
  iam_request_url=$(echo $signed_request | jq -r .iam_request_url)
  iam_request_body=$(echo $signed_request | jq -r .iam_request_body)
  iam_request_headers=$(echo $signed_request | jq -r .iam_request_headers)

  # Vault role name
  role_name=$1
  auth_path="$VAULT_ADDR/auth/cmig/aws/iam/login"

  data=$(
    cat <<EOF
{
  "role":"$role_name",
  "iam_http_request_method": "POST",
  "iam_request_url": "$iam_request_url",
  "iam_request_body": "$iam_request_body",
  "iam_request_headers": "$iam_request_headers"
}
EOF
  )

  http_notoken_req_string POST "$data" "$auth_path" 
}


get_access_token(){
  vault_iam_login $1 | jq -r '.auth.client_token'
}