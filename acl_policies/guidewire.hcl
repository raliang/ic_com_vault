#Secrets KV v1
path "cmig/guidewire/v1"
{
  capabilities = ["list"] 
}

path "cmig/guidewire/v1/*"
{
  capabilities = ["read", "list"] 
}