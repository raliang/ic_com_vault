# List auth methods
path "sys/auth"
{
  capabilities = ["read"]
}

# List existing policies
path "sys/policies/acl"
{
  capabilities = ["read"]
}

# Create and manage ACL policies
path "sys/policies/acl/*"
{
  capabilities = ["read", "list"]
}

# List, create, update, and delete key/value secrets
path "sys/mount"
{
  capabilities = ["read"]
}

# List, create, update, and delete key/value secrets
path "cmig/*"
{
  capabilities = ["create", "read", "update", "delete", "list"]
}
